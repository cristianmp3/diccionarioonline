    

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario Online</title>
    </head>
    <body>
        <div class="text-center">
            <h1>Diccionario Online</h1>
        
        </div>
        <form name="form" action="controller" method="POST">
            <div class="container" align="center">
                <div class="row">
                    <h2>Ingrese Palabra</h2>
                    <div class="col-md-4 col-md-offset-4">
                        <input maxlength="20" type="text" name="palabra" required id="palabra" class="form-control" placeholder="Escriba una Palabra...">
                    </div>
                </div>
            </div>
            <br>
            <div class="container"  align="center">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <input class="btn btn-primary" type="submit" value="Consultar"/>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
