package root.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import root.dao.DiccionarioJpaController;
import root.persistence.entities.Diccionario;

@Path("/diccionario")
public class DiccionarioRest {

    DiccionarioJpaController dao = new DiccionarioJpaController();

    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public String listarTodo(@HeaderParam("api-key") String apikey, @HeaderParam("api-id") String apiId, @PathParam("palabra") String palabra) {
        List<Diccionario> lista = dao.findDiccionarioEntities();
        System.out.println("palabra : " + palabra);
        System.out.println("api_key : " + apikey);
        System.out.println("api_id : " + apiId);
        final String language = "es";
        final String word = palabra;
        final String fields = "definitions";
        final String strictMatch = "true";
        final String word_id = word.toLowerCase();        
        final String urlOxford = "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word_id + "?" + "fields=" + fields + "&strictMatch=" + strictMatch;
        
        try 
        {
            URL url = new URL(urlOxford);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();   
            urlConnection.setRequestProperty("Content-Type", "application/json charset=UTF-8");
            urlConnection.setRequestProperty("app_id", apiId);
            urlConnection.setRequestProperty("app_key", apikey);

           
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            System.out.println("Respuesta Oxford : " + reader);
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }
            System.out.println("Respuesta 2 Oxford : " + stringBuilder.toString());
            return stringBuilder.toString();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
            return e.toString();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevaConsola(Diccionario diccionario) {
        dao.create(diccionario);
        return "Registro Guardado OK";
    }
}
